package cuffdiffTools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import net.imglib2.RealPoint;
import net.imglib2.collection.KDTree;
import net.imglib2.neighborsearch.KNearestNeighborSearchOnKDTree;
import net.imglib2.neighborsearch.NearestNeighborSearchOnKDTree;

public class getDifferentialExpressed 
{	
	HashMap<String, DifferentailExpressionGroup> candidateMap;
	HashMap<String, DifferentailExpressionGroup> controlMap;
	
	ArrayList<DifferentialExpressionEntry> entryList, background; 
	ArrayList<DifferentialExpressionEntry> increasedControl, appearedControl, decreasedControl, disappearedControl; 
	
	public getDifferentialExpressed( BufferedReader in, HashMap<String, Boolean> argSet )
	{
		// Parse the exp.diff file
		setEntryList( in ); 
		
		setChanged( entryList ); 
		
		//Write candidate groups to files
		writeCandidateGroups(); 
		if( argSet.get("-c" ) ) 
		{
			setControlGroups(); 
			//Write control groups to files
			writeControlGroups(); 
		}
		
		
		// Exit system
		System.exit( 0 ); 
	}
	
	/**
	 * Writes candidate groups to files 
	 */
	public void writeCandidateGroups()
	{
		for( String key : candidateMap.keySet() )
		{
			candidateMap.get( key ).write(); 
		}
	}
	/**
	 * Writes control groups to files 
	 */
	public void writeControlGroups()
	{
		for( String key : controlMap.keySet() )
		{
			controlMap.get( key ).write(); 
		}
	}
	
	/**
	 * Takes the FPKM of the first sample and extracts a expression matched sample from the background group. 
	 * Works on each group of candidates (increased, decreased, appeared, disappeared)
	 */
	public void setControlGroups()
	{
		HashMap<String, DifferentailExpressionGroup> controlGroups = new HashMap<String, DifferentailExpressionGroup>();  
		sortList( this.background );
		
		KDTree<DifferentialExpressionEntry> kdTree = new KDTree<DifferentialExpressionEntry>( this.background, this.background );
		NearestNeighborSearchOnKDTree<DifferentialExpressionEntry> nearestNeighborSearch = new NearestNeighborSearchOnKDTree<DifferentialExpressionEntry>(kdTree ); 
		
		for( String k : candidateMap.keySet() )
		{ 
			String key = k + "Control"; 
			controlGroups.put(key, new DifferentailExpressionGroup( key ) );
			
			
			if( k.equals("appeared") )
			{
				int size = candidateMap.get( k ).getEntryList().size(); 
				KNearestNeighborSearchOnKDTree<DifferentialExpressionEntry> nNearestNeighborSearch = new KNearestNeighborSearchOnKDTree<DifferentialExpressionEntry>(kdTree, size); 
				nNearestNeighborSearch.search( new RealPoint(0) );
				for( int i = 0; i < size; i++ )
				{
					controlGroups.get( key ).addEntry( nNearestNeighborSearch.getSampler( i ).get() ); 
				}
			}
			else 
			{
				for(DifferentialExpressionEntry e :  candidateMap.get( k ).getEntryList() )
				{
					nearestNeighborSearch.search( e ); 
					controlGroups.get( key ).addEntry( nearestNeighborSearch.getSampler().get() ); 
				}
			}
			
			this.controlMap = controlGroups;  
		}	
	}
	
	/**
	 * sorts differential expression entry list
	 * @param list
	 */
	public void sortList( ArrayList<DifferentialExpressionEntry> list )
	{
		Collections.sort(list, new DifferentialExpressionEntryComparatorByFPKMSample1() ); 
	}
	
	/**
	 * Parsing the differential expression file. 
	 * @param in
	 */
	public void setEntryList( BufferedReader in )
	{
		try
		{
			this.entryList = ReadDifferentialExpressionFile.readDifferentialExpressionFile( in ); 
		}
		catch (IOException e) 
		{
			System.err.println( "Cannot read the provided exp.diff file. " + e ); 
			System.exit( 1 ); 
		}
	}
	
	/**
	 * Filters the entryList for entries which are significantly changed. Splits those into increased or decreased in sample2 compared to sample1 
	 * @param entryList 
	 */
	public void setChanged( ArrayList<DifferentialExpressionEntry> entryList )
	{
		HashMap<String, DifferentailExpressionGroup> candidateGroups = new HashMap<String, DifferentailExpressionGroup>(); 
		
		candidateGroups.put("increased", new DifferentailExpressionGroup( "increased" ) ); 
		candidateGroups.put("appeared", new DifferentailExpressionGroup( "appeared" ) );
		candidateGroups.put("decreased", new DifferentailExpressionGroup( "decreased" ) );
		candidateGroups.put("disappeared", new DifferentailExpressionGroup( "disappeared" ) ); 
		
		this.background = new ArrayList<DifferentialExpressionEntry>(); 
		
		for( DifferentialExpressionEntry e : entryList )
		{
			if( e.value_1 == 0 && e.value_2 > 0 )
			{
				candidateGroups.get( "appeared" ).addEntry( e ) ; 				
			}
			else if( e.value_1 > 0 && e.value_2 == 0 )
			{
				candidateGroups.get( "disappeared" ).addEntry( e ) ; 				
			}
			else if( e.isSignificant())
			{ 
				if( e.log2FoldChange > 0 )
				{
					candidateGroups.get( "increased" ).addEntry( e ) ; 
				}
				else 
				{
					candidateGroups.get( "decreased" ).addEntry( e ) ; 
				}
			}
			else
			{
				background.add( e ); 				
			}
		}
		
		
		for( String key : candidateGroups.keySet() )
		{ 
			sortList( candidateGroups.get( key ).getEntryList() ); 
		}
		
		this.candidateMap = candidateGroups; 
	}
	
	public static void main(String[] args) throws IOException 
	{
		
		// Initiate and read input stream
		InputStreamReader inp = new InputStreamReader( System.in ); 
		BufferedReader in = new BufferedReader( inp );
		//BufferedReader in = TextFileAccess.openFileRead( "/Users/carrillo/Documents/workspace/GeneExpressionToolbox/samples/isoform_exp.diff" ); 
		
		// Print help text if no file is piped to script
		final String info = "\n#######################\n" +
		"java -jar getDifferentailExpressed [Options]*\n" +
		"#######################\n" + 
		"Filters a differential expression file (X_exp.diff, X=isoform, gene, cds or tss) produced by cuffdiff by significance of change.\n" +
		"Please provide the differential expression file via STDIN.\n" +
		"It writes the entries for increase or decrease of sample1 vs sample2 in separate files (sampleID2Increase.diff, sampleID2Decrease.diff).\n" +
		"It further writes the entries which are not present in sample1, but in sample2 (sampleID2Appear.diff) or vice versa (sampleID2Disappear.diff).\n" +
		"\nOptions:\n" +
		"-c\tWrites control groups for increase and decrease samples. These control groups contain the same number of genes and have expression values in sample1 as similar as possible.\n\n";
		if( !in.ready() )
		{	
			System.err.println( info ); 
			System.exit( 1 ); 
		}
		else 
		{
			// Set potential parameters
			HashMap<String, Boolean> argSet = new HashMap<String, Boolean>(); 
			argSet.put("-c", false ); 
			
			// Read passed arguments
			for( String arg : args )
			{
				Boolean b = argSet.get( arg );
				if( b != null )
				{
					argSet.put( arg, true );
				}
				else 
				{
					System.err.println( info ); 
					System.exit( 1 ); 
				}
			}
			new getDifferentialExpressed( in, argSet ); 
		}
	}

}
