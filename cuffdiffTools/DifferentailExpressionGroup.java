package cuffdiffTools;

import inputOutput.TextFileAccess;

import java.io.PrintWriter;
import java.util.ArrayList;

public class DifferentailExpressionGroup 
{
	public String id;
	public ArrayList<DifferentialExpressionEntry> entryList; 
	
	public DifferentailExpressionGroup( final String id )
	{
		this.id = id;
		this.entryList = new ArrayList<DifferentialExpressionEntry>(); 
	}
	
	public void write()
	{
		final String fileName = getSample2ID() + getId() + ".diff"; 
		PrintWriter out = TextFileAccess.openFileWrite( fileName );
		
		for( DifferentialExpressionEntry e : getEntryList() )
		{
			out.println( e ); 
		}
		
		out.close(); 
	}
	
	public String getId() { return this.id; }; 
	public void addEntry( final DifferentialExpressionEntry e ) { getEntryList().add( e ); } 
	public ArrayList<DifferentialExpressionEntry> getEntryList() { return this.entryList; }
	public String getSample2ID() { return getEntryList().get( 0 ).getSample_2(); } 
}
