package cuffdiffTools;

import java.util.Comparator;

public class DifferentialExpressionEntryComparatorByFPKMSample1 implements
		Comparator<DifferentialExpressionEntry> 
{

	@Override
	public int compare(DifferentialExpressionEntry arg0,
			DifferentialExpressionEntry arg1) 
	{
		if( arg0.getValue_1() > arg1.getValue_1() )
			return 1; 
		else if ( arg0.getValue_1() < arg1.getValue_1() )
			return -1; 
		else 
			return 0;
	}

}
