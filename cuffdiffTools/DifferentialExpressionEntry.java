package cuffdiffTools;

import net.imglib2.RealLocalizable;

public class DifferentialExpressionEntry implements RealLocalizable
{
	public String row; 
	public String test_id, gene_id, gene, locus, sample_1, sample_2, status;
	float value_1, value_2, log2FoldChange, test_stat, p_value, q_value;
	boolean significant; 
	
	public DifferentialExpressionEntry( final String row )
	{
		 setAll( row ); 
	}
	
	/**
	 * Set all instance parameters. 
	 * @param row
	 */
	public void setAll( final String row )
	{
		this.row = row; 
		String[] entries = row.split("\t"); 
		this.test_id = entries[ 0 ]; 
		this.gene_id = entries[ 1 ]; 
		this.gene = entries[ 2 ];
		this.locus = entries[ 3 ];
		this.sample_1 = entries[ 4 ];
		this.sample_2 = entries[ 5 ];
		this.status= entries[ 6 ];
		
		this.value_1 = Float.parseFloat( entries[ 7 ] );
		this.value_2 = Float.parseFloat( entries[ 8 ] );
		this.log2FoldChange = Float.parseFloat( entries[ 9 ] );
		this.test_stat = Float.parseFloat( entries[ 10 ] );
		this.p_value = Float.parseFloat( entries[ 11 ] );
		this.q_value = Float.parseFloat( entries[ 12 ] );
		
		if( entries[13].equals("yes") )
			this.significant = true; 
		else 
			this.significant = false; 
	}
	
	public boolean isSignificant() { return this.significant; }
	public float getLog2FoldChange() { return this.log2FoldChange; }
	public float getValue_1() { return this.value_1; }
	public float getValue_2() { return this.value_2; }
	public String getSample_2() { return this.sample_2; }
	
	public String toString()
	{
		return this.row; 
	}

	@Override
	public double getDoublePosition(int arg0) 
	{
		return getValue_1(); 
	}

	@Override
	public float getFloatPosition(int arg0) {
		return getValue_1();
	}

	@Override
	public void localize(float[] arg0) {
		arg0[0] = getValue_1();
		
	}

	@Override
	public void localize(double[] arg0) {
		arg0[0] = getValue_1();
		
	}

	@Override
	public int numDimensions() {
		return 1;
	}
}
