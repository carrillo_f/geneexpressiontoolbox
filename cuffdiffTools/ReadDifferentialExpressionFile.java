package cuffdiffTools;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

public class ReadDifferentialExpressionFile 
{
	public static ArrayList<DifferentialExpressionEntry> readDifferentialExpressionFile( BufferedReader in ) throws IOException
	{
		ArrayList<DifferentialExpressionEntry> entries = new ArrayList<DifferentialExpressionEntry>(); 
		
		String row; 
		int line = 0; 
		while( in.ready() )
		{
			row = in.readLine();
			// Skip header line 
			if( line > 0 )
				entries.add( new DifferentialExpressionEntry( row ) );
			line++; 
		}
		
		return entries; 
	}

}
